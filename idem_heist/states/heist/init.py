from typing import Dict

__contracts__ = ["resource"]


# TODO should this be a reverse sub?
# So i can say, heist.salt.minion.present(), but have "salt" populate a variable in this present state
# TODO do this as an auto_state
# TODO ideally that would use the service start/stop, is that implemented for salt minion?
# TODO we need some contracts in heist so that we can depend on functionality being present, how to get/list/create/delete/update heist services
async def present(
    hub,
    ctx,
    name: str,
    heist_manager: str,
    remote: Dict[str, str],
    artifact_version: str = None,
    **kwargs
):
    """
    Generic

    .. code-block:: sls

        my_instance_bootstrap:
          heist.heist_manager.present:
            - artifact_version:
            - remote:
              host: 192.168.4.4
              username: fred
              password: freds_password

    Salt

    .. code-block:: sls

        my_instance_bootstrap:
          heist.salt.minion.present:
            - artifact_version:
            - remote:
              host: 192.168.4.4
              username: fred
              password: freds_password
    """
    remotes = {name: remote}
    # We need more fine-tuned control than this, can I spin up salt, then close the connection but leave the service running?
    await hub.heist[heist_manager].run(
        remotes=remotes, artifact_version=artifact_version, **kwargs
    )


async def absent(hub, ctx, name: str, resource):
    ...
    # The heist connection should enable a service and keep it running until we re-connect and explicitly kill it and clean it up


async def describe(hub, ctx):
    ...
    # Is there a meaningful way to do a `describe` for this?
